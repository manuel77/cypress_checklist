const elements = {
    TITULO: {
        ARTIGO: '.article-title',
        DESTAQUE: '#What-you’ll-learn',
        ICONE_CAP: '.fa-graduation-cap',
        ICONE_CHECK: '.fa-check-square-o'
    },
    NOTA: {
        CONTEUDO: '.note',
    },
    IDIOMA: {
        DEFAULT: '#lang-select',
    },
    BUSCAR: {
        TERMO: '#search-input',
    }

}

export default elements;
