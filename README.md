#  Checklist: Cypress  #

  

[![Cypress](https://www.cypress.io/static/33498b5f95008093f5f94467c61d20ab/1200e/cypress-logo.webp)](https://www.cypress.io/)

  
Cypress is a next generation front end testing tool built for the modern web. We address the key pain points developers and QA engineers face when testing modern applications.

This project will access the [https://www.checklistfacil.com/](https://www.checklistfacil.com/) website in order to test the "Talk to an Expert" functionality.
 

# To install run:

    npm install

#### To run the tests in Headless mode, run:

    npx cypress run

#### To run with Test Runner, run:

    npx cypress open

  
## About choosing the test:

The test itself is super simple, as I wanted to focus more on the basis for reuse and flexibility when building new tests.

#### On 'Assertions - Texts and fields':

I did more with the objective of showing what a test with low performance is like, although it already uses Locators (ids.js) which helps a little if you do a refactoring in the front, for example.

> Advantage: this way the tests are faster, otherwise, when the ids (or class, or div, or xpath....) are directly in the test, this one is slower and breaks more easily.
> Disadvantage: Difficult to read and high maintenance test if there are multiple test scenarios on the same screen and some functionality change is required.

#### On 'Fill and selects - options form with constants':

I used the structure of the previous test to assemble commands (commands.js) by "subject" of the form. I used constants (const.js).

> Advantage: if the fields filled in do not change frequently, it is a good idea to leave it because if it is necessary to change it at some point, it will only be changed in 1 place. It is also possible to use some "Faker" to generate this data.
> Disadvantage: little diversity of data inserted in the fields and the filled values are not visible in the test texts.

#### On "Fill and selects - options form with errors"

The commands previously assembled by "subject" are now field by field, with the content being inserted directly into the test, simulating invalid fields.

I took the opportunity to show a scenario 'forcing' an error and validating that the feedback was presented to the user. The feedback text is printed to the test console log.

> Advantage: I used the same commands used within the larger commands for "subject" and had more flexibility in the data entered in the tests.




